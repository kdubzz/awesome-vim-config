" autocmd BufEnter * EnableBlameLine

set guicursor=
set relativenumber
set nu
set nohlsearch
set hidden
set noerrorbells
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab
set smartindent
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set termguicolors
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect
set colorcolumn=80
set signcolumn=yes
set diffopt+=vertical
set mouse=a

set cmdheight=3
set updatetime=50
set shortmess+=c

set statusline=
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\

syntax on
syntax enable

call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'yuki-yano/fzf-preview.vim', { 'branch': 'release/rpc' }
Plug 'gruvbox-community/gruvbox'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'preservim/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'airblade/vim-rooter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'windwp/nvim-autopairs'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'tpope/vim-rails'
Plug 'pechorin/any-jump.vim'
Plug 'mattn/emmet-vim'
Plug 'skywind3000/vim-preview'
Plug 'tveskag/nvim-blame-line'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Must install ack command-line tool for this plugin
Plug 'mileszs/ack.vim'

" Telescope Plugin
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
" Plug 'nvim-telescope/telescope.nvim'
" Plug 'nvim-telescope/telescope-fzy-native.nvim'
call plug#end()

colorscheme gruvbox
highlight Normal guibg=none

" Other worthy plugins
" nvim lsp
" nvim telescope
" treesitter
" undo
" fugitive

let mapleader = " "

let g:deoplete#enable_at_startup = 1
let g:airline_theme="solarized_flood"

" Automatically close brackets - mapping
inoremap {<CR> {<CR>}<Esc>O
inoremap [<CR> [<CR>]<Esc>O
inoremap (<CR> (<CR>)<Esc>O

" Set blame line
nnoremap <leader>l :EnableBlameLine<CR>
nnoremap <leader>ll :DisableBlameLine<CR>

" Navigation & basic commands
nnoremap <leader>' :term<CR>
nnoremap <leader>f :Files<CR>
nnoremap <leader>s :w!<CR>
nnoremap <leader>q :q!<CR>
nnoremap <leader>w <C-w><C-w><CR>
nnoremap <leader>h :sp<CR>
nnoremap <leader>v :vsp<CR>
nnoremap <leader>u :UndotreeShow<CR>
" Delete current buffer
nnoremap <leader>d :bd<CR>

" See all buffers
nnoremap <leader>b :Buffers<CR>

" File searches using Ack command line tool
nnoremap <leader>a :Ack!<Space>

" Tabs & Buffers
nnoremap <leader><TAB> :tab sp<CR>
nnoremap \t :tabn<CR>

" Move back & forth between current line in current buffer
nnoremap <leader>o <C-O><C-O><CR>
nnoremap <leader>i <C-I><C-I><CR>

" NERD Tree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <leader>r :NERDTreeFind<CR>
nnoremap <C-n> :NERDTree<CR>

" YCM Completer goto definition
" nnoremap <leader>gd :YcmCompleter GoToDefinitionElseDeclaration<CR>

" Git commands
nnoremap <leader>git :Git<CR>
nnoremap <leader>gv :Gvdiffsplit<CR>

" AnyJump
nnoremap <leader>j :AnyJump<CR>

fun! TrimWhiteSpace()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfun

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

augroup KDUBZZ
  autocmd!
  autocmd BufWritePre * :call TrimWhiteSpace()
augroup END
