set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" NERD Tree
Plugin 'preservim/nerdtree'
" NERD Commenter
Plugin 'preservim/nerdcommenter'
" Vim-Rainbow
Plugin 'frazrepo/vim-rainbow'
let g:rainbow_active=1
" Lightline
Plugin 'itchyny/lightline.vim'
" FZF plugin for for fuzzy searching
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
" Slim Template (Rails) Support for Vim 
Plugin 'slim-template/vim-slim.git'
" Solarized theme colouring for vim
Plugin 'lifepillar/vim-solarized8'
" Vim Git Gutter Plugin
Plugin 'vim-gitgutter'
" Nerdtree Git Plugin
Plugin 'Xuyuanp/nerdtree-git-plugin'
" Tmux Plugin
Plugin 'tmux-plugins/vim-tmux'
" Matchit Plugin
Plugin 'tmhedberg/matchit'
" Vim AutoPairs
Plugin 'ferranpm/vim-autopairs'
" Ack Plugin
Plugin 'mileszs/ack.vim'
" ALE plugin for async syntax checking
Plugin 'w0rp/ale'
" Auto-complete in Vim
Plugin 'wokalski/autocomplete-flow'
" Awesome Vim Colourschemes
Plugin 'flazz/vim-colorschemes'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" ======== Plug Plugins ============
" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'leafgarland/typescript-vim'
Plug 'vim-utils/vim-man'
Plug 'lyuts/vim-rtags'
Plug 'git@github.com:kien/ctrlp.vim.git'
Plug 'git@github.com:Valloric/YouCompleteMe.git'
Plug 'mbbill/undotree'

call plug#end()
colorscheme gruvbox

hi ColorColumn ctermbg=0 term=bold cterm=bold guibg=lightgrey

if executable('rg')
  let g:rg_derive_root='true' 
endif

let mapleader = " "
let g:netrw_browse_split=2
let g:netrw_banner=0

syntax on
syntax enable

python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup

set nowrap
set laststatus=2 " Always display the statusline in all windows
set showtabline=2 " Always display the tabline, even if there is only one tab set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
set t_Co=256
set tabstop=2
set shiftwidth=0
set number
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set colorcolumn=80
set incsearch
set noerrorbells
set smartindent
set softtabstop=2
set cursorline
set modifiable
set guifont=Mono:h17
set background=dark
set autoindent
set expandtab
set autoread

" Set Custom Key Mappings

" Git Key Mappings
" Git add file
map \ga :Gw <Enter>
" Show Git Diffs

function GitDiff()
  :silent write
  :silent exec '!git diff --color=always -- ' . expand('%:p') . ' | less --RAW-CONTROL-CHARS'
  :redraw!
endfunction

nnoremap <leader>gj :diffget //3<CR>
nnoremap <leader>gu :diffget //2<CR>
nnoremap <leader>gf :G<CR>

" Invoke Git Diff for all files
map \gd :!git diff <Enter> 

nnoremap <leader>s :w! <Enter>
nnoremap <leader>q :q! <Enter>
nnoremap <leader>t :term <Enter>
nnoremap <leader>t :term <Enter>
nnoremap <leader>v :vsp <Enter>
nnoremap <leader>ss :sp <Enter>
nnoremap <leader>f :FZF <Enter>
nnoremap <leader>n :NERDTree <Enter>
nnoremap <leader>r :NERDTreeFind<cr>
nnoremap <leader>] :NERDTreeFind<CR>
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>u :UndotreeShow<CR>
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
nnoremap <leader>ps :Rg<SPACE>

" YCM keymaps
nnoremap <silent> <leader>gd :YcmCompleter GoTo<CR>
nnoremap <silent> <leader>gf :YcmCompleter FixIt<CR>

" Moving back andd forth between files
nnoremap <leader>oo <C-O><C-O>
nnoremap <leader>ii <C-I><C-I>
